﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour {

	public GameObject torch;
	public GameObject holdingPosition;
	public GameObject placeholderTorch;

	Ray ray;
	RaycastHit hit;
	public bool holdingTorch = true;
	bool holdingObject = false;
	float tempTimePressed;

	GameObject object_to_pickup;

    private GameObject old_object;

    
	void Update(){
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if(Physics.Raycast(ray, out hit)){
			if (hit.distance  <= 3 ){
				if(Input.GetMouseButtonDown(0)){
                    hit.transform.gameObject.SendMessage("OnPointerClick", SendMessageOptions.DontRequireReceiver);

                    if (hit.collider.transform.tag == "obj" && !holdingObject){
					
						object_to_pickup = hit.collider.transform.gameObject;
						PickupObject();
						tempTimePressed = Time.time;						
					}
					if(hit.collider.transform.tag == "torch"){
						object_to_pickup = hit.collider.transform.gameObject;
						PickupTorch();
					}
				}
			
	            if (hit.transform.gameObject != old_object){
	                if (old_object){
	                    old_object.SendMessage("OnPointerExit", SendMessageOptions.DontRequireReceiver);
	                }
	                hit.transform.gameObject.SendMessage("OnPointerEnter", SendMessageOptions.DontRequireReceiver);
	                old_object = hit.transform.gameObject;
	            }
            }else{
            	hit.transform.gameObject.SendMessage("OnPointerExit", SendMessageOptions.DontRequireReceiver);
            	old_object = null;
            }
        }
        else{
            if (old_object){
                old_object.SendMessage("OnPointerExit", SendMessageOptions.DontRequireReceiver);
                old_object = null;
            }
        }

        if (holdingObject && Input.GetMouseButtonDown(0) && (Time.time - tempTimePressed > 1f)){
			DropObject();
		}
		if(holdingObject){
			object_to_pickup.GetComponent<Rigidbody>().velocity = holdingPosition.transform.position - object_to_pickup.transform.position;// this.GetComponent<Rigidbody>().velocity;
		}
	}

	void PickupTorch(){
		Destroy(object_to_pickup);
		holdingObject = false;
		holdingTorch = true;
		torch.SetActive(true);
	}

	void PickupObject(){
		if(holdingTorch){
			holdingTorch = false;
			torch.SetActive(false);
			Instantiate(placeholderTorch as GameObject, torch.transform.position, torch.transform.rotation); 
		}

		object_to_pickup.transform.parent = holdingPosition.transform;
		Rigidbody rb = object_to_pickup.GetComponent<Rigidbody>();
		rb.useGravity = false;
		rb.constraints = RigidbodyConstraints.FreezeRotation;
		holdingObject = true;
	}

	public void DropObject(){
		if(holdingTorch){
			holdingTorch = false;
			torch.SetActive(false);
			Instantiate(placeholderTorch as GameObject, torch.transform.position, torch.transform.rotation); 
		} else{
			object_to_pickup.transform.parent = null;
			Rigidbody rb = object_to_pickup.GetComponent<Rigidbody>();
			rb.useGravity = true;
			rb.constraints = RigidbodyConstraints.None;
			holdingObject = false;
		}
		
	}
}
