﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerStates : MonoBehaviour {

	PuzzleStateMachine puzzleStateMachine;
	Animator anim;
	PlayerInteraction playerInteraction;
	public GameObject spear;
	FirstPersonController charController;

	public AudioClip pain;
	public AudioClip scream;

	AudioSource audioSource;

	void Awake(){
		puzzleStateMachine = (PuzzleStateMachine)GameObject.FindObjectOfType<PuzzleStateMachine>();
		anim = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
		playerInteraction = GetComponent<PlayerInteraction>();
		charController = transform.parent.GetComponent<FirstPersonController>();
	}
	public void PlayerDeath(string causeOfDeath){
		puzzleStateMachine.SavePillarStates();
		charController.enabled = false;
		StartCoroutine(PlayerDeathAnimations(causeOfDeath));
	}

	IEnumerator PlayerDeathAnimations(string causeOfDeath){
		Color oldFogColor = new Color();
		ColorUtility.TryParseHtmlString("#4A4272FF", out oldFogColor);
		Color fogCollor = new Color();
		switch(causeOfDeath){
			case "spear":
				audioSource.clip = pain;
				audioSource.Play();
				spear.SetActive(true);
				yield return new WaitForSeconds(0.2f);
				fogCollor = Color.red;
				break;
			case "fire":
				fogCollor = new Color(0.2F, 0.3F, 0.4F);
				break;
			case "poison":
				yield return new WaitForSeconds(0.2f);
				fogCollor = Color.green;
				break;
			case "curse":
				audioSource.clip = scream;
				audioSource.Play();
				yield return new WaitForSeconds(0.2f);
				fogCollor = Color.black;
				break;
		}
		float elapsedTime = 0f;
		float totalTime = 0.8f;
		while(elapsedTime < totalTime){
			elapsedTime += Time.smoothDeltaTime / totalTime;			
			RenderSettings.fogColor = Color.Lerp(oldFogColor, fogCollor, elapsedTime);
			yield return null;
		}
		
		playerInteraction.DropObject();
		anim.enabled = true;
		anim.SetTrigger("death");
		yield return new WaitForSeconds(5f);
		SceneManager.LoadScene("Main");
	}
}
