﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour {

	public AudioClip impactSound; 
    AudioSource audioSource;

    void Awake(){
    	audioSource = GetComponent<AudioSource>();
    }

	public void PlayImpactSound(){
		Debug.Log("this gets called");
		audioSource.clip = impactSound;
		audioSource.volume = 1f;
		audioSource.PlayOneShot(impactSound, 1f);
		audioSource.volume = .3f;
	}
}
