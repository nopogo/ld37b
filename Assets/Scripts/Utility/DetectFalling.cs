﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectFalling : MonoBehaviour {


	AudioSource audioSource;
	public AudioSource audioSourceLoop;
	AudioClip rollingSound;

	Rigidbody rigidbody;
	float levelstarts;

	void Awake(){
		audioSource = GetComponent<AudioSource>();
		levelstarts = Time.time;
		rigidbody = GetComponent<Rigidbody>();
	}

	void OnCollisionStay(Collision collision){
		if(collision.transform.gameObject.name == "Floor" || collision.transform.gameObject.name == "PuzzleBody"){
			if(IsRolling() && !audioSourceLoop.isPlaying){
				audioSourceLoop.Play();	
			}
		}

		if(!IsRolling()){
			audioSourceLoop.Stop();
		}
	}

	void OnCollisionExit(Collision collision){
		if(audioSourceLoop.isPlaying){
			audioSourceLoop.Stop();
		}
	}

	bool IsRolling(){
		if(rigidbody.velocity.magnitude > 0.3f){
			return true;
		}
		return false;
	}

	void OnCollisionEnter(Collision col){
		if(col.collider.transform.tag == "floor" && Time.time-5f > levelstarts){
			audioSource.Play();
		}
	}
}
