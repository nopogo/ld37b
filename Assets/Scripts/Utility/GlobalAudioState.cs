﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalAudioState : MonoBehaviour {
	public static GlobalAudioState g;
	public bool isAudioAllowed = true;
	void Awake(){
		if(!g){
			g = this;
			DontDestroyOnLoad(gameObject);
		} else{
			Destroy(gameObject);
		}

		UpdateAllAudioListeners();
	}

	public void SetAudioAllowed(bool isAllowed){
		isAudioAllowed = isAllowed;
		UpdateAllAudioListeners();
	}

	void Update(){
		UpdateAllAudioListeners();
	}

	void UpdateAllAudioListeners(){
		AudioListener[] listeners = (AudioListener[])GameObject.FindObjectsOfType<AudioListener>();
		foreach(AudioListener l in listeners){
			l.enabled = isAudioAllowed;
		}
	}
}
