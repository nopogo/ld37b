﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightIntensityByDistance : MonoBehaviour {

  private Transform player;
  private Light moodLight;

	void Start () {
		player = GameObject.Find("FPSController(Clone)").transform;
    moodLight = gameObject.GetComponent<Light>();
	}

	void Update () {
    float distance = Vector3.Distance(player.position, transform.position);

    moodLight.intensity = distance / 6;

	}
}
