﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentObject : MonoBehaviour {
	public static PersistentObject p;
	
	void Awake(){
		if(!p){
			p = this;
			DontDestroyOnLoad(gameObject);
		} else{
			Destroy(gameObject);
		}
	}
}
