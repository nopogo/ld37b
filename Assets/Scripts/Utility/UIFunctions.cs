﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIFunctions : MonoBehaviour {

	GlobalAudioState globalAudioState;

	void Awake(){
		globalAudioState = (GlobalAudioState)GameObject.FindObjectOfType<GlobalAudioState>();
	}
	public void StartGame(){
		SceneManager.LoadScene("Main");
	}

	public void ExitGame(){
		Application.Quit();
	}

	public void ToggleAudio(Toggle toggle){
		globalAudioState.SetAudioAllowed(!toggle.isOn) ;
	}
}
