﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectInteraction : MonoBehaviour {

    public virtual void OnPointerClick()
    {
        //Debug.Log("Clicking at object [ " + transform.name + " ]");
    }

    public virtual void OnPointerEnter()
    {
     
        //Debug.Log("Pointing at object [ " + transform.name + " ]");
        GUICrosshair guicrosshair = GameObject.Find("Canvas/Crosshair").GetComponent<GUICrosshair>();
        guicrosshair.crosshairTexture = guicrosshair.crosshairInteraction;        
    }

    public virtual void OnPointerExit()
    {
        //Debug.Log("Stopped pointing at object [ " + transform.name + " ]");
        GUICrosshair guicrosshair = GameObject.Find("Canvas/Crosshair").GetComponent<GUICrosshair>();
        guicrosshair.crosshairTexture = guicrosshair.crosshairBasic;
    }
}
