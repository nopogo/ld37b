﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCollisionDetection : MonoBehaviour {
	GameObject music;

	void Start(){
		music = GameObject.FindWithTag("music");
	}


	void OnCollisionEnter(Collision col){
		Destroy(music);
		StartCoroutine(FadeOut());
	}



	IEnumerator FadeOut () {
		AudioSource audioSource = GetComponent<AudioSource>();
        float startVolume = audioSource.volume;
        while (audioSource.volume > 0) {
            audioSource.volume -= startVolume * Time.deltaTime / 1f;
 
            yield return null;
        }
 
        audioSource.Stop ();
        audioSource.volume = startVolume;
    }

}
