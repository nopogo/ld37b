﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTrigger : MonoBehaviour {

    public GameObject TriggeredObject;

    void OnTriggerEnter(Collider colliderObject)
    {
        if (colliderObject.tag == "Player")
        {
            TriggeredObject.SendMessage("Triggered");
        }
    }

    void OnTriggerStay(Collider other)
    {
    }

    void OnTriggerExit(Collider other)
    {
    }
}
