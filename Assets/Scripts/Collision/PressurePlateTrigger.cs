﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlateTrigger : MonoBehaviour {

	 public GameObject tile;

    AudioSource audioSource;
    Animator anim;
    private bool isTriggered;

    void Awake(){
        audioSource = GetComponent<AudioSource>();
        anim = tile.GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider colliderObject){
      	if (colliderObject.tag == "Player" && !isTriggered){
            isTriggered = true;
            audioSource.Play();
            anim.SetTrigger("triggered");
        }
    }
}
