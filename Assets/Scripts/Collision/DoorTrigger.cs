﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour {

	bool triggered = false;
	public GameObject door;
	
	void OnTriggerEnter(Collider col){
		if(!triggered){
			triggered = true;
			door.GetComponent<Rigidbody>().useGravity = true;
			door.GetComponent<AudioSource>().Play();
		}
	}		
}
