﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollPuzzleInteraction : ObjectInteraction {

  public GameObject connectedSwitchPlank;



  new public virtual void OnPointerClick() {
    base.OnPointerClick();
    PlayerUsedPanel();
    }

  public void PlayerUsedPanel() {
    GetComponent<AudioSource>().Play();

    Debug.Log("Interacted with a panel!");
    if (connectedSwitchPlank.activeSelf == true ) {
      connectedSwitchPlank.SetActive(false);
    } else {
      connectedSwitchPlank.SetActive(true);
    }
  }
}
