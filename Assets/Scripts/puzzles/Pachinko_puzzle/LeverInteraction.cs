﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverInteraction : ObjectInteraction {

	public Animator trapDoorAnimator;

	public bool state = false;
	Animator anim;


	void Awake(){
		anim = GetComponent<Animator>();
		if(state){
			anim.SetTrigger("close");
			trapDoorAnimator.SetTrigger("close");
		}
	}

	new public virtual void OnPointerClick() {
	    base.OnPointerClick();
	    StartCoroutine(PlayerUsedPanel());
    }

    public IEnumerator PlayerUsedPanel() {
    	state = !state;
    	if(state){
			anim.SetTrigger("close");
		}else{
			anim.SetTrigger("open");
		}
		yield return new WaitForSeconds(0.7f);
		if(state){
			trapDoorAnimator.SetTrigger("close");
		}else{
			trapDoorAnimator.SetTrigger("open");
		}
		
		GetComponent<AudioSource>().Play();
    }
    	
}
