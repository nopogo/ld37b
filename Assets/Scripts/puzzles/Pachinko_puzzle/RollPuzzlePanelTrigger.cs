﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollPuzzlePanelTrigger : MonoBehaviour {

  private PlayerStates playerStates;

  public bool isSolution = false;
  Pillar pillar;
  AudioSource audioSource;

  void Awake(){
    audioSource = gameObject.transform.parent.gameObject.GetComponent<AudioSource>();
    pillar = GameObject.Find("Pillar back left").GetComponent<Pillar>();
  }

  void OnTriggerEnter(Collider col) {
    if (col.gameObject.name == "ball"){
      if(isSolution){
        pillar.SetSolved(true);
      }else{
        audioSource.Play();
        playerStates = (PlayerStates)GameObject.FindObjectOfType<PlayerStates>();
        playerStates.PlayerDeath("poison");
      }
    }
  }
}
