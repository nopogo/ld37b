﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class useItem : ObjectInteraction {

	public string deathMessage = "test";
	public bool isCorrect = false;
	public GameObject curse;
	PlayerStates playerStates;
	Pillar pillar;
	bool solved = false;

	void Awake(){
		pillar = GameObject.Find("Pillar front left").GetComponent<Pillar>();
	}

	new public virtual void OnPointerClick() {
		if(!solved){
			base.OnPointerClick();
    		PickupObject();
		}
	}
	void PickupObject(){
		if(isCorrect){
			pillar.SetSolved(true);
			solved = true;
		}else{
			curse.SetActive(true);
			playerStates = (PlayerStates)GameObject.FindObjectOfType<PlayerStates>();
			playerStates.PlayerDeath("curse");
		}
	}
}
