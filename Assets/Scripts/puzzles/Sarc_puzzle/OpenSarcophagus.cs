﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OpenSarcophagus : ObjectInteraction {

    new public virtual void OnPointerClick()
    {
        base.OnPointerClick();
        
        StartCoroutine(OpenSarcophagusFunction());
    }

    new public virtual void OnPointerEnter()
    {
        base.OnPointerEnter();
    }

    new public virtual void OnPointerExit()
    {
        base.OnPointerExit();
    }

    IEnumerator OpenSarcophagusFunction()
    {
        transform.GetComponent<AudioSource>().Play();
        transform.parent.GetComponent<Animator>().SetTrigger("Opening");
        yield return new WaitForSeconds(1);
        transform.GetComponent<AudioSource>().Play();
    }
}
