﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AltarPuzzleInteraction : ObjectInteraction {

  public GameObject DeathCause;
  public bool isCorrectAltarPanel = false;
  public bool isLit = false;


  private Transform fire;
  private PlayerStates playerStates;
  private PlayerInteraction playerInteraction;
  private AltarPillarButtonInteraction altarButton;
  private AudioSource audioSource;

  void Awake(){
    altarButton = (AltarPillarButtonInteraction)GameObject.FindObjectOfType<AltarPillarButtonInteraction>();
    
    audioSource = GetComponent<AudioSource>();
  }

  void Start() {
    fire = transform.GetChild(0);
  }

  new public virtual void OnPointerClick() {
        playerInteraction = (PlayerInteraction)GameObject.FindObjectOfType<PlayerInteraction>();
        base.OnPointerClick();
        if(!isLit && !playerInteraction.holdingTorch){
          return;
        }
        isLit = !isLit;        
        fire.gameObject.SetActive(isLit);
        if(isLit){
          audioSource.Play();
        } else{
          audioSource.Stop();
        }
        altarButton.UpdateState();
  }

  public virtual void OnPointerEnter()
  {
      base.OnPointerEnter();
      GUICrosshair guicrosshair = GameObject.Find("Canvas/Crosshair").GetComponent<GUICrosshair>();
      guicrosshair.crosshairTexture = guicrosshair.crosshairTorch;
  }

}
