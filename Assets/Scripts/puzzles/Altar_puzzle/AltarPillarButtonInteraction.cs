﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarPillarButtonInteraction : ObjectInteraction {

  public GameObject DeathCause;
  public bool isSafeToUse = false;
  public int nrOfBowlsLit = 0;

  public AltarPuzzleInteraction bowl1 ;
  public AltarPuzzleInteraction bowl2 ;
  public AltarPuzzleInteraction bowl3 ;

  private PlayerStates playerStates;
  private Pillar pillar;

  private bool isCorrect = false;
  private bool solved = false;
  private AudioSource audioSource;


  void Awake(){
    pillar = GameObject.Find("Pillar front right").GetComponent<Pillar>();
    audioSource = GetComponent<AudioSource>();
  }

  new public virtual void OnPointerClick() {
      if(!solved){
         base.OnPointerClick();
        audioSource.Play();
        PlayerUsedAltarPillarButton();
        Debug.Log("Player used altar pillar button");
      }
  }

  public void UpdateState(){
    if(!bowl1.isLit && bowl2.isLit && !bowl3.isLit){
      if(!isCorrect){
        audioSource.Play();
      }
      isCorrect = true;
       transform.eulerAngles = new Vector3(0, 90, 0);
    }else{
      if(isCorrect){
        audioSource.Play();
      }
      isCorrect = false;
      transform.eulerAngles = new Vector3(0, -90, 0);
    }
  }


  void PlayerUsedAltarPillarButton() {
    if (isCorrect) {
      Debug.Log("the correct altar panel is lit, the puzzle is solved and pillar activates!");
      pillar.SetSolved(true);
      solved = true;
    } else {
      // The correct altar panel is not lit, kill the player
      Debug.Log("The correct altar panel is not lit, YOU DIE!");
      DeathCause.SetActive(true);

      playerStates = (PlayerStates)GameObject.FindObjectOfType<PlayerStates>();
      playerStates.PlayerDeath("fire");
    }
  }

}
