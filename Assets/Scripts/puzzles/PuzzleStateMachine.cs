﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleStateMachine : MonoBehaviour {

	public bool pillar_state_pillar_front_left;
	public bool pillar_state_pillar_front_right;
	public bool pillar_state_pillar_back_left;
	public bool pillar_state_pillar_back_right;

	public Pillar pillar_front_left;
	public Pillar pillar_front_right;
	public Pillar pillar_back_left;
	public Pillar pillar_back_right;

	public GameObject ufo;

	AudioSource audioSource;
	Animator roofAnimator; 
	

	void Awake(){
		pillar_front_left.SetSolved(pillar_state_pillar_front_left);
		pillar_front_right.SetSolved(pillar_state_pillar_front_right);
		pillar_back_left.SetSolved(pillar_state_pillar_back_left);
		pillar_back_right.SetSolved(pillar_state_pillar_back_right);
		audioSource = GetComponent<AudioSource>();
	}

	public void SavePillarStates(){
		pillar_state_pillar_front_left = pillar_front_left.solvedState;
		pillar_state_pillar_front_right = pillar_front_right.solvedState;
		pillar_state_pillar_back_left = pillar_back_left.solvedState;
		pillar_state_pillar_back_right = pillar_back_right.solvedState;
	}

	public void CheckVictory(){
		if (pillar_front_left.solvedState && pillar_front_right.solvedState && pillar_back_left.solvedState){ // 3 not 4 pillars work  && pillar_back_right.solvedState
			StartCoroutine(WinSequence());
		}
	}

	IEnumerator WinSequence(){
		roofAnimator = GameObject.FindWithTag("roof").GetComponent<Animator>();
		AudioSource roofSound = GameObject.FindWithTag("roof").GetComponent<AudioSource>();
		GameObject player = GameObject.FindWithTag("Player");
		GameObject godray = GameObject.FindWithTag("godray");

		Image fadeToWhite = GameObject.FindWithTag("fadetowhite").GetComponent<Image>();
		Image credits = GameObject.FindWithTag("credits").GetComponent<Image>();
		Debug.Log("we won the game by winning");
		
		yield return new WaitForSeconds(1f);
		roofAnimator.Play("Roof");
		godray.SetActive(false);
		roofSound.Play();
		yield return new WaitForSeconds(5f);
		GameObject ufoTemp = Instantiate(ufo);
		yield return new WaitForSeconds(3f);
		float elapsedTime = 0f;
		float totalTime = 2f;
		Color oldFogColor = new Color();
		ColorUtility.TryParseHtmlString("#4A4272FF", out oldFogColor);
		while(elapsedTime < totalTime){
			player.GetComponent<CharacterController>().enabled = false;
			player.GetComponent<Rigidbody>().isKinematic = false;
			player.GetComponent<Rigidbody>().useGravity = false;
			player.GetComponent<Rigidbody>().AddForce(Vector3.up * 1f, ForceMode.Acceleration);
			elapsedTime += Time.smoothDeltaTime / totalTime;			
			RenderSettings.fogColor = Color.Lerp(oldFogColor, Color.white, elapsedTime);
			fadeToWhite.color = new Color(1,1,1, Mathf.Lerp(0f,1f, elapsedTime/2));
			yield return null;
		}
		fadeToWhite.color = new Color(1,1,1,1);
		Destroy(ufoTemp);

		elapsedTime = 0f;
		totalTime = 2f;
		while(elapsedTime < totalTime){
			elapsedTime += Time.smoothDeltaTime / totalTime;
			credits.color = new Color(1,1,1, Mathf.Lerp(0f,1f, elapsedTime/2));
			yield return null;
		}		
		audioSource.Play();
		credits.color = new Color(1,1,1,1);
		yield return new WaitForSeconds(7f);
		SceneManager.LoadScene("NewMainMenu");
		audioSource.Stop();
	}
}
