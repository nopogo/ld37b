﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pillar : MonoBehaviour {
	public bool solvedState = false;
	Animator anim;
	AudioSource audioSource;
	PuzzleStateMachine puzzleStateMachine;
	public GameObject glow;

	void Awake(){
		puzzleStateMachine = (PuzzleStateMachine)GameObject.FindObjectOfType<PuzzleStateMachine>();
		anim = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
	}


	public void SetSolved(bool state){
		solvedState = state;
		if(solvedState){
			glow.SetActive(true);
			audioSource.Play();
			anim.SetTrigger("turn_pillar");
			puzzleStateMachine.CheckVictory();

		}
	}
}
