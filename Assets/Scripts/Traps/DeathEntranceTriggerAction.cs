﻿using UnityEngine;
using System.Collections;

public class DeathEntranceTriggerAction : MonoBehaviour
{
    //Variable to check if a object has been fired
    private bool Shots_Fired = false;

    //Drag in the Bullet Emitter from the Component Inspector.
    public GameObject Bullet_Emitter;

    //Drag in the Bullet Prefab from the Component Inspector.
    public GameObject Bullet;

    //Enter the Speed of the Bullet from the Component Inspector.
    float Bullet_Forward_Force =100f;

    void Triggered(){
        if (!Shots_Fired){
           StartCoroutine(TriggerDelay());  
        }
    }

    IEnumerator TriggerDelay(){
        yield return new WaitForSeconds(0.2f);
        Shots_Fired = true;

        //The Bullet instantiation happens here.
        GameObject Temporary_Bullet_Handler;
        Temporary_Bullet_Handler = Instantiate(Bullet, Bullet_Emitter.transform.position, Bullet_Emitter.transform.rotation) as GameObject;

        //Sometimes bullets may appear rotated incorrectly due to the way its pivot was set from the original modeling package.
        //This is EASILY corrected here, you might have to rotate it from a different axis and or angle based on your particular mesh.
        Temporary_Bullet_Handler.transform.Rotate(Vector3.left * 90);

        //Retrieve the Rigidbody component from the instantiated Bullet and control it.
        Rigidbody Temporary_RigidBody;
        Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody>();


        //Tell the bullet to be "pushed" forward by an amount set by Bullet_Forward_Force. 

        // Temporary_RigidBody.AddForce(transform.forward * Bullet_Forward_Force);
        Temporary_RigidBody.velocity = Vector3.left * Bullet_Forward_Force;

        //Basic Clean Up, set the Bullets to self destruct after 10 Seconds, I am being VERY generous here, normally 3 seconds is plenty.
        //Destroy(Temporary_Bullet_Handler, 10.0f);
    }


}