﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : MonoBehaviour {
    PlayerStates playerStates;
    PlayerAudio playerAudio;
    public float movementSpeed = 10f;

    void Awake(){
        playerStates = (PlayerStates)GameObject.FindObjectOfType<PlayerStates>();
        playerAudio = (PlayerAudio)GameObject.FindObjectOfType<PlayerAudio>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player"){
            playerAudio.PlayImpactSound();
            playerStates.PlayerDeath("spear");
        }
    }
}
